const newObject = {
    firstname : 'Harry',
    lastname : 'Potter Holt',
    destination : 'Hogwarts React conf',
    occupation : 'Deve-wizard Avucado',
    spell : 'Vimulus Rendurus!'
}

let {firstname, lastname, destination, occupation, spell} = newObject

console.log(firstname, lastname, destination, occupation, spell)