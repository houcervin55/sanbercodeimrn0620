function descendingTen(num = -1){
    angka = ''
    if (num == -1){
        angka += num
    } else {
        for (var i = num; i > num -10; i--){
            angka += i + ' '
        }
    }
    return angka
}

function ascendingTen(num = -1){
    angka = ''
    if (num == -1){
        angka += num
    } else {
        for (var i = num; i < num + 10; i++){
            angka += i + ' '
        }
    }
    return angka
}

function conditionalAscDesc(ref = -1, check = -1){
    angka = ''
    if (ref == -1 || check == -1){
        console.log(-1)
    } else if(check %2 == 0){
        angka += descendingTen(ref)
    } else {
        angka += ascendingTen(ref)
    }
    return angka
}

function ularTangga(){
    var angka = 100
    var urut = ''
    var i 
    for (var a = 0; a < 10; a++){
        for ( i = angka ; i > angka - 10; i--){
            urut += i + ' '
        }
        console.log(urut)
        urut = ''
        angka -= 10
    }
}

console.log(descendingTen(90))
console.log(ascendingTen(1))
console.log(conditionalAscDesc(3,6))
ularTangga()