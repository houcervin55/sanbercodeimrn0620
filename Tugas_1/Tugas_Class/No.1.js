// Release 0

class Animal {
    constructor(name){
        this.name = name
    }

    get legs(){
        let legs = 4
        return legs
    }

    set legs(x){
        this.legs = x
    }

    get cold_bloded(){
        return false
    }

    set cold_bloded(x){
        return x
    }
}

var sheep = new Animal('shaun')

console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_bloded)

// Release 1

class Ape extends Animal{
    constructor(name){
        super(name)
    }

    get legs (){
        return 2
    }

    set legs(x){
        return x
    }

    yell(){
        console.log('Auoooo')
    }

 
}

class Frog extends Animal{
    constructor(name){
        super(name)
    }

    jump(){
        console.log('hop hop')
    }
}

var sungokong = new Ape('Kera Sakti')
sungokong.yell()

var kodok = new Frog ('buduk')
kodok.jump()