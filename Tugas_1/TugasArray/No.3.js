function range(x,y,z = 1){
    let number = []
    
    for(let i = x; i <= y; i+=z){
        number.push(i)
    }
    
    let sum = 0
    for(let j = 0; j < number.length; j++){
        sum += number[j]
    }
    return sum
}

console.log(range(1,3))