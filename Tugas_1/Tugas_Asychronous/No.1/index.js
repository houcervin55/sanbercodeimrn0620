var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
var time = 10000
for (let i = 0; i < books.length; i ++){
    readBooks(time, books[i], function(callback){
        console.log(callback)
    })
    time -= books[i].timeSpent
}

