const newFunction = (firstname, lastname) => {
    return  {
        firstname,
        lastname,
        fullname : () => console.log(`${firstname} ${lastname}`)
    }
}

newFunction('William', 'imoh').fullname()